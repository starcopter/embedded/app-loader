######################################
# target
######################################
TARGET = app-loader


######################################
# building variables
######################################
# optimization
# 	O0 -    Unoptimized ( faster compile time )
# 	O1 -    General optimizations no speed/size tradeoff
# 	O2 -    More aggressive size and speed optimization
# 	O3 -    Favor speed over size
# 	Os -    Favor size over speed
# 	Ofast - O3 plus inaccurate math
# 	Og -    Optimize for debugging experience
OPT ?= Os
-include submodules/shared-utilities/git-vars.mk
# FreeRTOS port selection
FREERTOS_PORT = ARM_CM4F

#######################################
# paths
#######################################
# Build path
BUILD_DIR = build
# FreeRTOS base directory
FREERTOS_DIR = submodules/freertos/kernel
# DSDL source file directory; in here we should only have root namespaces and nothing else
NUNAVUT_DSDL_DIR = dsdl
# output directory for Nunavut
NUNAVUT_OUT_DIR = gen

######################################
# source
######################################
# C sources
C_SOURCES = \
$(sort $(wildcard src/*.c)) \
submodules/shared-utilities/canard/fdcan.c \
submodules/shared-utilities/canard/canard.c \
submodules/shared-utilities/canard/node.c \
submodules/shared-utilities/canard/transport.c \
submodules/shared-utilities/sclib/assert.c \
submodules/shared-utilities/sclib/console.c \
submodules/shared-utilities/sclib/timekeeper.c \
submodules/shared-utilities/sclib/log.c \
submodules/shared-utilities/sclib/flash.c \
submodules/shared-utilities/sclib/image.c \
submodules/shared-utilities/sclib/crc.c \
submodules/shared-utilities/sclib/shared_ram.c \
submodules/shared-utilities/sclib/dcb.c \
submodules/shared-utilities/sclib/registry.c \
submodules/shared-utilities/sclib/image_desc_registers.c \
$(FREERTOS_DIR)/portable/GCC/$(FREERTOS_PORT)/port.c \
$(FREERTOS_DIR)/portable/MemMang/heap_4.c \
$(sort $(wildcard $(FREERTOS_DIR)/*.c))

# ASM sources
ASM_SOURCES =

# Used DSDL Namespaces
DSDL_NAMESPACES = uavcan

#######################################
# binaries
#######################################
EXPECTED_GCC_VERSION := 10.2.1
CC = arm-none-eabi-gcc
AS = arm-none-eabi-gcc -x assembler-with-cpp
CP = arm-none-eabi-objcopy
SZ = arm-none-eabi-size
READELF = arm-none-eabi-readelf
HEX = $(CP) -O ihex --gap-fill 0xff
BIN = $(CP) -O binary --strip-all --gap-fill 0xff
GDB = arm-none-eabi-gdb
MKDIR = mkdir -p

#######################################
# CFLAGS
#######################################
# cpu
CPU = -mcpu=cortex-m4

# fpu
FPU = -mfpu=fpv4-sp-d16

# float-abi
FLOAT-ABI = -mfloat-abi=hard

# mcu
MCU = $(CPU) -mthumb $(FPU) $(FLOAT-ABI)

# macros for gcc
# C defines
C_DEFS = \
-DNUNAVUT_ASSERT=ASSERT \
-DCANARD_ASSERT=ASSERT \
-DBUILD_DATE=$(BUILD_DATE)UL \
-DGIT_COMMIT="$(GIT_COMMIT)" \
-DGIT_COMMIT_ARR="$(GIT_COMMIT_ARR)" \
-DGIT_CLEAN=$(GIT_CLEAN) \
-DVERSION_MAJOR=$(VERSION_MAJOR) \
-DVERSION_MINOR=$(VERSION_MINOR) \
-DVERSION_PATCH=$(VERSION_PATCH) \
-DVERSION_COMMITS=$(VERSION_COMMITS) \
-DSTM32G431xx

# log level overrides from command line/ENV vars; may strongly affect code size
ifdef LOG_LEVEL_OVERRIDE
C_DEFS += -DconfigLOG_LEVEL_OVERRIDE=${LOG_LEVEL_OVERRIDE}
endif
ifdef LOG_LEVEL_MIN
C_DEFS += -DconfigLOG_LEVEL_MIN=${LOG_LEVEL_MIN}
endif

# C includes
C_INCLUDES = \
-Isrc \
-Isubmodules/shared-utilities \
-Isubmodules/shared-utilities/CMSIS/Include \
-Isubmodules/shared-utilities/CMSIS/Device/ST/STM32G4xx/Include \
-I$(FREERTOS_DIR)/include \
-I$(FREERTOS_DIR)/portable/GCC/$(FREERTOS_PORT) \
-I$(NUNAVUT_OUT_DIR)

# compile gcc flags
CFLAGS = $(MCU) $(C_DEFS) $(C_INCLUDES) -$(OPT) -fdata-sections -ffunction-sections -g3 -gdwarf-2 -fstack-usage

# compiler warnings and errors
CFLAGS += -Wall -Wextra -Werror -Wshadow -Wdouble-promotion -Wformat=2 -Wformat-truncation=2 -Wundef -fno-common
CFLAGS += -Wno-unused-parameter -Wno-error=unused-variable -Wno-error=unused-but-set-variable -Wno-error=unused-function

# Generate dependency information
CFLAGS += -MMD -MP -MF"$(@:%.o=%.d)"

# ensure reproducibility in file paths
CFLAGS += -fmacro-prefix-map="$(CURDIR)"=. -fdebug-prefix-map="$(CURDIR)"=.


#######################################
# LDFLAGS
#######################################
# link script
LDSCRIPT ?= $(TARGET).ld

# libraries
LIBS = -lc -lm -lnosys
LIBDIR =
LDFLAGS = $(MCU) -specs=nano.specs -T$(LDSCRIPT) $(LIBDIR) $(LIBS) -Wl,-Map=$(BUILD_DIR)/$(TARGET).map,--cref -Wl,--gc-sections -Wl,--build-id -Wl,--print-memory-usage

# default action: build all
all: version-check $(BUILD_DIR)/$(TARGET).elf build-id $(BUILD_DIR)/$(TARGET).hex $(BUILD_DIR)/$(TARGET).bin sha256
.PHONY: version-check sha256

GCC_VERSION := $(strip $(shell $(CC) -dumpversion))
version-check:
ifneq ($(GCC_VERSION),$(EXPECTED_GCC_VERSION))
	$(error This project is intended to be compiled against $(EXPECTED_GCC_VERSION). You are using $(GCC_VERSION) ¯\_(ツ)_/¯)
else
	$(info Found matching GCC $(EXPECTED_GCC_VERSION))
endif

sha256: $(BUILD_DIR)/$(TARGET).elf $(BUILD_DIR)/$(TARGET).hex $(BUILD_DIR)/$(TARGET).bin
	@sha256sum $^


#######################################
# Nunavut DSDL transpiler
#######################################
_NUNAVUT_AVAILABLE_ROOT_NAMESPACES = $(wildcard $(NUNAVUT_DSDL_DIR)/*)
NUNAVUT_FLAGS  = --target-language c --target-endianness=little
NUNAVUT_FLAGS += --enable-serialization-asserts
NUNAVUT_FLAGS += --language-standard C11
NUNAVUT_FLAGS += --pp-run-program clang-format --pp-run-program-arg=-i
NUNAVUT_FLAGS += --outdir=$(NUNAVUT_OUT_DIR)
NUNAVUT_FLAGS += $(_NUNAVUT_AVAILABLE_ROOT_NAMESPACES:%=--lookup-dir=%)
NUNAVUT_HEADERS = $(DSDL_NAMESPACES:%=$(NUNAVUT_OUT_DIR)/%)

.PHONY: setup nnvg
setup:
ifneq (${CI}, true)
	git submodule update --init --recursive
	PIPENV_VERBOSITY=-1 pipenv sync --dev
else
	pipenv sync
endif

nnvg: $(NUNAVUT_HEADERS)

$(NUNAVUT_OUT_DIR)/%: $(NUNAVUT_DSDL_DIR)/%
	@$(MKDIR) $(@D)
	@echo Transpiling DSDL Namespace $<
	@PIPENV_VERBOSITY=-1 pipenv run nnvg $(NUNAVUT_FLAGS) $<
	@touch $@


#######################################
# build the application
#######################################
# list of objects
OBJECTS = $(addprefix $(BUILD_DIR)/,$(C_SOURCES:.c=.o))
vpath %.c $(sort $(dir $(C_SOURCES)))
# list of ASM program objects
OBJECTS += $(addprefix $(BUILD_DIR)/,$(ASM_SOURCES:.s=.o))
vpath %.s $(sort $(dir $(ASM_SOURCES)))

$(BUILD_DIR)/%.o: %.c $(NUNAVUT_HEADERS) Makefile
	@$(MKDIR) $(@D)
	@echo Compiling $*.c
	@$(CC) -c $(CFLAGS) -D__FILENAME__=\"$(notdir $<)\" -Wa,-a,-ad,-alms=$(BUILD_DIR)/$(<:.c=.lst) $*.c -o $@

# force rebuild header
$(BUILD_DIR)/src/main.o: FORCE
FORCE:

$(BUILD_DIR)/%.o: %.s Makefile
	@$(MKDIR) $(@D)
	@echo Assembling $*.s
	@$(AS) -c $(CFLAGS) -D__FILENAME__=\"$(notdir $<)\" $*.s -o $@

$(BUILD_DIR)/$(TARGET).elf: $(OBJECTS) Makefile $(LDSCRIPT) submodules/shared-utilities/tools/patch-image.py
	@echo Linking $@
	@$(CC) $(OBJECTS) $(LDFLAGS) -o $@
	@$(BIN) $@ $(@:.elf=.bin-unpatched)
	@PIPENV_VERBOSITY=-1 pipenv run python submodules/shared-utilities/tools/patch-image.py $@

$(BUILD_DIR)/%.hex: $(BUILD_DIR)/%.elf
	@$(MKDIR) $(@D)
	@echo Building $@
	@$(HEX) $< $@

$(BUILD_DIR)/%.bin: $(BUILD_DIR)/%.elf
	@$(MKDIR) $(@D)
	@echo Building $@
	@$(BIN) $< $@

.PHONY: elfsize
elfsize: $(BUILD_DIR)/$(TARGET).elf $(BUILD_DIR)/$(TARGET).hex $(BUILD_DIR)/$(TARGET).bin
	@$(SZ) $<

.PHONY: build-id
build-id: $(BUILD_DIR)/$(TARGET).elf
	@$(READELF) -n $< | grep "Build ID"


#######################################
# device interaction
#######################################

.PHONY: reset flash run-gdbserver attach halt continue console
reset:
	pyocd reset
flash: $(BUILD_DIR)/$(TARGET).bin
	pyric device flash $<
run-gdbserver:
	pyocd gdbserver --connect attach
attach:
	$(GDB) --se $(BUILD_DIR)/$(TARGET).elf --ex "tar ext :3333"
halt:
	pyocd commander -c halt
continue:
	pyocd commander -c continue
SERIAL_PORT ?= /dev/ttyACM0
BAUDRATE ?= 1000000
console:
	pyserial-miniterm --exit-char 3 $(SERIAL_PORT) $(BAUDRATE) | tee console.log


#######################################
# clean up
#######################################
.PHONY: clean clean-all
clean:
	-rm -fR $(BUILD_DIR)

clean-all: clean
	-rm -fR $(NUNAVUT_OUT_DIR)


#######################################
# dependencies
#######################################
SHELL:=/usr/bin/bash -O globstar
-include $(shell [[ -d $(BUILD_DIR) ]] && ls $(BUILD_DIR)/**/*.d)
