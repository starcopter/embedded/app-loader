# App Loader

This is the application loader, the second of three components comprising the firmware update architecture outlined in [Memfault's blog article](https://interrupt.memfault.com/blog/device-firmware-update-cookbook#dfu-failures-should-not-brick-the-device).

The components are, or will be eventually:

0. the [Bootloader][]
1. the App Loader (this project)
2. the main application, e.g. [G4-Nucleo][]
3. the Loader Updater

[bootloader]: https://gitlab.com/starcopter/embedded/bootloader
[g4-nucleo]: https://gitlab.com/starcopter/embedded/g4-nucleo

[[_TOC_]]

## Memory Layout

The App Loader is not natively executable, it has to be launched by the [Bootloader][].

```
0x08020000 ┌─────────────────────┐
           │     2K NVM Data     │ ◄─────  EEPROM-like storage for persistent application data
0x0801f800 ├─────────────────────┤
           │                     │
           .         84K         .         arbitrary number of page-aligned applications
           .  Application Space  . ◄─────  every application has to start with an image header
           .  Page (2K) Aligned  .         the app loader discovers bootable apps and launches them
           │                     │
0x0800a800 ├─────────────────────┤
           │                     │         application loader and updater, launched by bootloader
           │    40K App Loader   │ ◄─────  either launches or updates apps in application space
           │                     │         communicates with image file server via UAVCAN
0x08000800 ├─────────────────────┤
           │    512 Bytes DCB    │ ◄─────  read-only device configuration block
0x08000600 ├─────────────────────┤
           │   1.5K Bootloader   │
0x08000000 └─────────────────────┘
```

The app loader communicates with the bootloader and other applications via a shared block in RAM.
See the [bootloader documentation][shared ram] for details.
```
0x20008000 ┌──────────────────────┐
           │                      │         Application RAM (stack, heap, noinit, etc.)
           .        31 KiB        . ◄─────  private to each application
           .   Application RAM    .         initialized on application boot
           │                      │         Bootloader does not touch this
0x20000400 ├──────────────────────┤
           │ 512 Bytes Bootloader │ ◄─────  Bootloader application RAM, .bss and stack
0x20000200 ├──────────────────────┤
           │ 512 Bytes Shared RAM │ ◄─────  Shared RAM persisted across reboots
0x20000000 └──────────────────────┘
```

[shared ram]: https://gitlab.com/starcopter/embedded/bootloader/-/tree/stm32g431xb?ref_type=heads#shared-ram

### Device Configuration Block (DCB)

Device-constant information like the hardware release version or a target description are stored in the Device Configuration Block.
The DCB shares a flash page with the immutable bootloader and thus can never be changed in a device's lifetime.
It is implemented in [sclib/dcb.h][dcb.h]:

[dcb.h]: https://gitlab.com/starcopter/embedded/shared-utilities/blob/06bdedc85141e27163e579935942e7b191a72a42/sclib/dcb.h

```c
enum DeviceConfigVersion {
    eDEV_CONFIG_VERSION_0       = 0,
    eDEV_CONFIG_VERSION_CURRENT = eDEV_CONFIG_VERSION_0
};

/* Hardware Version Specification in Little-Endian Order, sortable as u16 */
typedef struct HardwareVersion {
    const uint8_t minor;  // 1 through 255 (Altium style)
    const char    major;  // 'A' through 'Z' (Altium style)
} HWVersion_t;

typedef struct DeviceConfigurationV0 {
    const uint32_t magic;    // should be DEVICE_CONFIG_MAGIC (0x676663a5)
    const uint32_t version;  // should be eDEV_CONFIG_VERSION_CURRENT
    const uint32_t size;     // config block length in bytes, e.g. sizeof(struct DeviceConfigurationV0)

    const char        name[52];  // null-terminated device/platform name, as in uavcan.node.GetInfo.1.0.name
    const HWVersion_t release;   // device/platform hardware release

    const uint32_t crc;  // CRC-32 of the configuration block up to here
} DeviceConfig_t;
```

### Image Metadata

The App Loader (as well as apps in the application space) start with an image header containing metadata about the image.
The first five and the last word are relevant for the bootloader to [launch the application][launching images], the content in between is extensible.

As of image header version 3 (current), the header contains an application name and hardware and software versions and compatibility information,
which are described in more detail in the section [Image Compatibility Check](#image-compatibility-check) below.
Additionally, the header includes the Git hash, the build time and a [GNU Build ID][build-id] in the notes section following the header.

```c
enum ImageHeaderVersion {
    eHEADER_VERSION_2       = 2,
    eHEADER_VERSION_3       = 3,
    eHEADER_VERSION_CURRENT = eHEADER_VERSION_3
};

/* Software Version Specification in Little-Endian Order, sortable as u32 */
typedef struct SoftwareVersion {
    const uint8_t commits;  // Git Commits after Version Tag, see `git describe`
    const uint8_t patch;    // Semantic Versioning
    const uint8_t minor;    // Semantic Versioning
    const uint8_t major;    // Semantic Versioning
} SWVersion_t;

typedef struct ImageHeaderV3 {
    /* header's header, this is expected not to change */
    const uint32_t        image_magic;   // should be IMAGE_MAGIC (0x3063737f)
    const uint32_t* const header_start;  // image base address, absolute
    const uint32_t* const vector_table;  // vector table address, absolute
    const uint32_t        header_size;   // total header size in bytes
    const uint32_t* const image_end;     // image end address (first non-image byte), absolute

    /* header's implementation specific body, (header_size - 24) bytes */
    const uint8_t     image_hdr_version;   // should be eHEADER_VERSION_CURRENT
    const uint8_t     _reserved;           // reserved
    const uint16_t    clean_build : 1;     // this build stems from a clean worktree (git)
    const uint16_t    _flags      : 15;    // reserved
    const SWVersion_t version;             // software version
    const SWVersion_t min_upgrade_from;    // earliest software version to safely upgrade from
    const SWVersion_t max_downgrade_from;  // latest software version to safely downgrade from
    const HWVersion_t target_hardware;     // target hardware version
    const HWVersion_t min_hardware;        // earliest compatible hardware version
    const uint32_t    time_utc;            // build (or commit/release) time UTC epoch
    const uint8_t     git_sha[20];         // git hash, byte by byte
    const char        name[52];            // null-terminated application name, as in uavcan.node.GetInfo.1.0.name

    /* header's tail, has to be the last word */
    const uint32_t header_crc;  // CRC of the header up to here
} ImageHeader_t;
```

[launching images]: https://gitlab.com/starcopter/embedded/bootloader#applications
[build-id]: https://interrupt.memfault.com/blog/gnu-build-id-for-firmware

## Device Firmware Update

When the App Loader receives a [DFU Request via Shared RAM][dfu request] it starts the updater task instead of scanning the application space for executable images.
A firmware update proceeds roughly as follows:

1. copy the DFU context (image path, server node ID) from shared ram and reset DFU request
1. request first chunk (containing the header) from server
1. upon receiving the first chunk:
   1. ensure the header itself is valid
   1. ensure this update is permitted (see [below](#image-compatibility-check))
   1. unlock flash memory and erase pages required for the image
   1. start the flash writer task
   1. save the first chunk for later, do **not** write it into flash just yet
1. load image from server:
   1. request next chunk, wait for response
   1. send chunk to flash writer task for actual programming
   1. continue
1. when the last chunk has been received:
   1. send held back first chunk to the flash writer
   1. wait for all bytes to be written
   1. verify written image in flash (CRC)
1. reboot processor

[dfu request]: https://gitlab.com/starcopter/embedded/bootloader#dfu-request

### Image Compatibility Check

Before the old application is erased to make space for the new image, several compatibility checks are performed to guard against breaking updates.
The logic is implemented in `bool checkUpdatePermitted()` in [dfu.c](src/dfu.c):

1. if the new image's header is broken **reject**
1. if the new image overlaps with the app loader or exceeds the flash address range **reject**
1. if the new image's header version is not understood by the updater **reject**
1. if the new image's name does not start with the target name from DCB **reject**
1. if the target hardware release is not within the new image's `min_hardware` and `target_hardware` **reject**
1. if the old image is corrupt/broken **accept**
1. if the old image's header version is not understood **accept**
1. if the old image's software version is not within the new image's `min_upgrade_from` and `max_downgrade_from` **reject**
1. otherwise **accept**
