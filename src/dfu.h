/**
 * @file dfu.h
 * @author Lasse Fröhner <lasse@starcopter.com>
 * @brief Device Firmware Update Module
 * @version 0.1
 * @date 2021-06-24
 *
 * @copyright Copyright (c) 2021 starcopter GmbH
 *
 * This software is distributed under the terms of the MIT License.
 *
 */

#pragma once

void start_DFU(void);
