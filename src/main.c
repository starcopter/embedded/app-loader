/**
 * @file main.c
 * @author Lasse Fröhner <lasse@starcopter.com>
 * @brief Application Loader
 * @date 2023-03-30
 *
 * @copyright Copyright (c) 2023 starcopter GmbH
 *
 * This software is distributed under the terms of the MIT License.
 *
 */

#include "main.h"

#include <FreeRTOS.h>
#include <task.h>

#include <sclib/crc.h>
#include <sclib/registry.h>
#include <sclib/console.h>
#include <sclib/timekeeper.h>
#include <canard/fdcan.h>

#define LOG_LEVEL LOG_LEVEL_TRACE
#include <sclib/log.h>

extern void* __VECTOR_TABLE;

ImageHeader_t image_hdr __attribute__((section(".image_hdr"))) = {
    .image_magic  = IMAGE_MAGIC,
    .header_start = (uint32_t*) &image_hdr,
    .vector_table = (uint32_t*) &__VECTOR_TABLE,
    .header_size  = sizeof(ImageHeader_t),
    .image_end    = (uint32_t*) &__image_end,

    .image_hdr_version = eHEADER_VERSION_CURRENT,
    .clean_build       = GIT_CLEAN ? 1 : 0,  // set in Makefile
    .version =
        {
            .major   = VERSION_MAJOR,   // set in Makefile
            .minor   = VERSION_MINOR,   // set in Makefile
            .patch   = VERSION_PATCH,   // set in Makefile
            .commits = VERSION_COMMITS  // set in Makefile
        },
    .min_upgrade_from   = {.major = 0, .minor = 0, .patch = 0, .commits = 0},
    .max_downgrade_from = {.major = VERSION_MAJOR, .minor = VERSION_MINOR, .patch = 255, .commits = 255},
    .target_hardware    = {.major = 'A', .minor = 2},
    .min_hardware       = {.major = 'A', .minor = 2},
    .time_utc           = BUILD_DATE,      // set in Makefile
    .git_sha            = GIT_COMMIT_ARR,  // set in Makefile
    .name               = "com.starcopter.nucleo.g4.app-loader",

    .header_crc = 0x12344321,  // populated after linking
};

void init_app_loader(void) {
    PERIPH_BIT(RCC->AHB2ENR, RCC_AHB2ENR_GPIOAEN_Pos)    = 1;
    PERIPH_BIT(RCC->AHB2ENR, RCC_AHB2ENR_GPIOBEN_Pos)    = 1;
    PERIPH_BIT(RCC->APB1ENR1, RCC_APB1ENR1_FDCANEN_Pos)  = 1;
    PERIPH_BIT(RCC->APB1ENR1, RCC_APB1ENR1_USART2EN_Pos) = 1;

    /*
     * Pin Configuration
     * -----------------
     *
     * | Pin      | Signal    | Mode | OType | OSpeed    | Pull |  AF | Peripheral   | Function     |
     * | -------- | --------- | ---- | ----- | --------- | ---- | --: | ------------ | ------------ |
     * | **PA11** | `CAN_RX`  | AF   |       |           |      |   9 | `FDCAN1_RX`  | CAN          |
     * | **PA12** | `CAN_TX`  | AF   | PP    | Medium    |      |   9 | `FDCAN1_TX`  | CAN          |
     * | **PA13** | `SWDIO`   | AF   | PP    | Very High | Up   |   0 | `SWDIO_JTMS` |              |
     * | **PA14** | `SWCLK`   | AF   |       |           | Down |   0 | `SWCLK_JTCK` |              |
     * | **PB3**  | `UART_TX` | AF   | PP    | Medium    |      |   7 | `USART2_TX`  | Console      |
     * | **PG10** | `RST`     |      |       |           |      |     |              | Reset Button |
     *
     * */

    GPIOA->AFR[1] = (9ul << GPIO_AFRH_AFSEL11_Pos)        // CAN_RX         AF9: FDCAN1_RX
                  | (9ul << GPIO_AFRH_AFSEL12_Pos)        // CAN_TX         AF9: FDCAN1_TX
                  | (0ul << GPIO_AFRH_AFSEL13_Pos)        // SWDIO          AF0: SWDIO_JTMS
                  | (0ul << GPIO_AFRH_AFSEL14_Pos);       // SWCLK          AF0: SWCLK_JTCK
    GPIOB->AFR[0] = (7ul << GPIO_AFRL_AFSEL3_Pos);        // UART_TX        AF7: USART2_TX
    GPIOA->PUPDR = (1ul << GPIO_PUPDR_PUPD13_Pos)         // SWDIO_JTMS     Pull-Up
                 | (2ul << GPIO_PUPDR_PUPD14_Pos);        // SWCLK_JTCK     Pull-Down
    GPIOA->OSPEEDR = (1ul << GPIO_OSPEEDR_OSPEED12_Pos)   // CAN_TX         Medium Speed
                   | (3ul << GPIO_OSPEEDR_OSPEED13_Pos);  // SWDIO          Very-High Speed
    GPIOB->OSPEEDR = (1ul << GPIO_OSPEEDR_OSPEED3_Pos);   // UART_TX        Medium Speed
    GPIOA->MODER = (2ul << GPIO_MODER_MODE11_Pos)         // CAN_RX         AF
                 | (2ul << GPIO_MODER_MODE12_Pos)         // CAN_TX         AF
                 | (2ul << GPIO_MODER_MODE13_Pos)         // SWDIO          AF
                 | (2ul << GPIO_MODER_MODE14_Pos);        // SWCLK          AF
    GPIOB->MODER = (2ul << GPIO_MODER_MODE3_Pos);         // DEBUG_TX       AF

    crc_init();
    load_persistent_registers();
    console_init(0);

    // Select PLL "Q" clock (160 MHz) as FDCAN clock source
    MODIFY_REG(RCC->CCIPR, RCC_CCIPR_FDCANSEL, 0b01ul << RCC_CCIPR_FDCANSEL_Pos);
    fdcan_clk = 160000000UL;
}

int main(void) {
    init_timekeeper();
    init_app_loader();

    start_app_loader();
    vTaskStartScheduler();

    WTF();
    return 1;
}

void deinit_app_loader(void) {
    crc_deinit();

    printf("\r\n");
    console_flush();
    NVIC_DisableIRQ(configSTDOUT_USART_IRQn);
    NVIC_SetPriority(configSTDOUT_USART_IRQn, 0);

    SysTick->CTRL = 0UL;

    PERIPH_BIT(RCC->AHB1RSTR, RCC_AHB1RSTR_CRCRST_Pos)      = 1;
    PERIPH_BIT(RCC->AHB1RSTR, RCC_AHB1RSTR_CRCRST_Pos)      = 0;
    PERIPH_BIT(RCC->AHB2RSTR, RCC_AHB2RSTR_GPIOARST_Pos)    = 1;
    PERIPH_BIT(RCC->AHB2RSTR, RCC_AHB2RSTR_GPIOARST_Pos)    = 0;
    PERIPH_BIT(RCC->AHB2RSTR, RCC_AHB2RSTR_GPIOBRST_Pos)    = 1;
    PERIPH_BIT(RCC->AHB2RSTR, RCC_AHB2RSTR_GPIOBRST_Pos)    = 0;
    PERIPH_BIT(RCC->APB1RSTR1, RCC_APB1RSTR1_FDCANRST_Pos)  = 1;
    PERIPH_BIT(RCC->APB1RSTR1, RCC_APB1RSTR1_FDCANRST_Pos)  = 0;
    PERIPH_BIT(RCC->APB1RSTR1, RCC_APB1RSTR1_USART2RST_Pos) = 1;
    PERIPH_BIT(RCC->APB1RSTR1, RCC_APB1RSTR1_USART2RST_Pos) = 0;

    extern void SystemDeInit(void);
    SystemDeInit();
}

void USART2_IRQHandler(void) {
    console_irq_handler();
}

void vApplicationStackOverflowHook(TaskHandle_t xTask, char* pcTaskName) {
    LOG_CRITICAL("Stack Overflow for task %s at 0x%08lx", pcTaskName, (uint32_t) xTask);
    console_flush();
    WTF();
}

void vApplicationMallocFailedHook(void) {
    LOG_WARNING("call to pvPortMalloc() failed");
    console_flush();
    HALT_IF_DEBUGGING();
}
