/**
 * @file dfu.c
 * @author Lasse Fröhner <lasse@starcopter.com>
 * @brief Device Firmware Update Module
 * @version 0.1
 * @date 2021-06-24
 *
 * @copyright Copyright (c) 2021 starcopter GmbH
 *
 * This software is distributed under the terms of the MIT License.
 *
 */

#include "dfu.h"
#include "main.h"

#include <FreeRTOS.h>
#include <task.h>
#include <queue.h>

#include <canard/node.h>
#include <canard/canard.h>
#include <sclib/image.h>
#include <sclib/flash.h>
#include <sclib/dcb.h>
#include <sclib/registry.h>

#include <uavcan/file/Read_1_1.h>

#include <sclib/console.h>
#define LOG_LEVEL LOG_LEVEL_DEBUG
#include <sclib/log.h>

const TickType_t  REQUEST_RESPONSE_TIMEOUT  = pdMS_TO_TICKS(250);
const UBaseType_t FLASH_WRITER_QUEUE_LENGTH = 4;

static volatile uint32_t bytes_received;
static volatile uint32_t bytes_written;
static const uint32_t* volatile first_word;
static const uint32_t* volatile first_word_after;

typedef struct {
    uint32_t* write_addr;  // target address in flash; should be 8-byte aligned
    uint32_t  size;        // number of bytes in data[]; should be 8-byte aligned
    uint8_t   data[];      // [size] payload bytes
} ImageChunk_t;

static QueueHandle_t flash_writer_queue = NULL;

static void exit_DFU(void);
static void start_flash_writer(void);
static void UpdaterTask(void* param);
static void FlashWriterTask(void* param);
static bool is_update_permitted(const ImageHeader_t* const old_header, const ImageHeader_t* const new_header);

void start_DFU(void) {
    static TaskHandle_t handle = NULL;
    ASSERT(handle == NULL);  // This function may only be called once
    BaseType_t rv = xTaskCreate(UpdaterTask, "Updater", 1024, NULL, TaskPriorityHigh, &handle);
    ASSERT(rv == pdPASS);
}

static void exit_DFU(void) {
    vTaskSuspendAll();
    LOG_DEBUG("exit DFU, will restart now");
    flash_lock();
    console_flush();
    NVIC_SystemReset();
}

static void start_flash_writer(void) {
    static TaskHandle_t handle = NULL;
    if (handle != NULL) return;  // This function may only be called once
    BaseType_t rv = xTaskCreate(FlashWriterTask, "Flash Writer", 192, NULL, TaskPriorityIdle, &handle);
    ASSERT(rv == pdPASS);
}

static void UpdaterTask(void* param __unused) {
    char                 image_path[256];
    uint32_t             server_node_id;
    const ImageHeader_t* old_header;
    int                  path_len = shared_get_dfu_context(image_path, &server_node_id, &old_header);
    ASSERT(path_len > 0);

    {
        const UAVCANRegister_t* const reg = get_register_by_name("log_level.console");
        if (reg) {
            uint8_t* const level = (uint8_t*) reg->value;
            if (*level < LOG_LEVEL_DEBUG) {
                *level = LOG_LEVEL_DEBUG;
                reg->update_hook();
            }
        }
    }
    {
        const UAVCANRegister_t* const reg = get_register_by_name("log_level.uavcan");
        if (reg) {
            uint8_t* const level = (uint8_t*) reg->value;
            if (*level < LOG_LEVEL_INFO) {
                *level = LOG_LEVEL_INFO;
                reg->update_hook();
            }
        }
    }

    LOG_INFO("load %s from %lu", image_path, server_node_id);

    shared_reset_dfu_request();

    uavcan_file_Read_Request_1_1 request = {.path.path.count = path_len};
    strcpy((char*) &request.path.path.elements[0], image_path);
    uint8_t buf[uavcan_file_Read_Request_1_1_SERIALIZATION_BUFFER_SIZE_BYTES_];

    CanardTransferMetadata meta = {.priority       = CanardPrioritySlow,
                                   .transfer_kind  = CanardTransferKindRequest,
                                   .port_id        = uavcan_file_Read_1_1_FIXED_PORT_ID_,
                                   .remote_node_id = server_node_id,
                                   .transfer_id    = 0};

    flash_writer_queue  = xQueueCreate(FLASH_WRITER_QUEUE_LENGTH, sizeof(ImageChunk_t*));
    bool flash_unlocked = false;

    const uint32_t MAX_DFU_ATTEMPTS = 3;

    for (uint32_t dfu_attempt = 1; dfu_attempt <= MAX_DFU_ATTEMPTS; dfu_attempt++) {
        bytes_received   = 0;
        bytes_written    = 0;
        first_word       = NULL;
        first_word_after = NULL;
        request.offset   = 0;

        uint32_t       chunk_retry_counter = 0;
        const uint32_t MAX_CHUNK_ATTEMPTS  = 4;

        ImageChunk_t*  first_chunk = NULL;
        ImageHeader_t* new_header  = NULL;

        const TickType_t dfu_start = xTaskGetTickCount();

        size_t      size;
        int_fast8_t status;

        while (1) {
            size   = sizeof(buf);
            status = uavcan_file_Read_Request_1_1_serialize_(&request, buf, &size);
            ASSERT(status == NUNAVUT_SUCCESS);

            LOG_DEBUG("request chunk %lu",
                      (uint32_t) request.offset / uavcan_primitive_Unstructured_1_0_value_ARRAY_CAPACITY_);

            CanardRxTransfer* rx_transfer = node_call(&meta, size, uavcan_file_Read_Response_1_1_EXTENT_BYTES_, buf,
                                                      NULL, 1, REQUEST_RESPONSE_TIMEOUT);
            ++meta.transfer_id;

            if (!rx_transfer) {
                if (++chunk_retry_counter < MAX_CHUNK_ATTEMPTS) {
                    LOG_TRACE("Retry");
                    continue;
                }
                WTF();
            }

            chunk_retry_counter = 0;

            uavcan_file_Read_Response_1_1 response;
            size   = rx_transfer->payload_size;
            status = uavcan_file_Read_Response_1_1_deserialize_(&response, rx_transfer->payload, &size);
            ASSERT(status == NUNAVUT_SUCCESS);

            vPortFree(rx_transfer->payload);
            vPortFree(rx_transfer);

            ASSERT(response._error.value == uavcan_file_Error_1_0_OK);

            LOG_DEBUG("Bytes %lu-%lu received", (uint32_t) request.offset,
                      (uint32_t) (request.offset + response.data.value.count - 1));

            // response.data.value.count ceil()'ed to next 8-byte boundary
            const size_t  padded_size = (response.data.value.count + 7) & ~((size_t) 7);
            ImageChunk_t* chunk       = pvPortMalloc(sizeof(ImageChunk_t) + padded_size);

            chunk->size = padded_size;
            memcpy(chunk->data, response.data.value.elements, response.data.value.count);
            memset(&chunk->data[response.data.value.count], 0, padded_size - response.data.value.count);
            bytes_received += response.data.value.count;

            if (request.offset == 0) {
                first_chunk = chunk;
                new_header  = (ImageHeader_t*) chunk->data;
                ASSERT(response.data.value.count >= sizeof(ImageHeader_t));  // TODO: think about upward compatibility

                const bool header_ok = image_check(new_header, true, false);
                if (!header_ok) {
                    LOG_WARNING("Header CRC mismatch");
                    exit_DFU();
                }

                const bool update_permitted = is_update_permitted(old_header, new_header);
                if (!update_permitted) {
                    LOG_WARNING("Update not permitted");
                    exit_DFU();
                }

                LOG_INFO("valid header received");
                const uint32_t image_size_kib =
                    ((uint32_t) new_header->image_end - (uint32_t) new_header->header_start + 1023) >> 10;
                char image_version_string[VERSION_STRING_MAXSZ];
                image_render_version_string(image_version_string, &new_header->version, new_header->clean_build);
                LOG_NOTICE("Programming %s %s, %lu KiB", new_header->name, image_version_string, image_size_kib);
                chunk->write_addr = (uint32_t*) new_header->header_start;

                if (!flash_unlocked) {
                    flash_unlock();
                    flash_unlocked = true;
                }

                first_word                = new_header->header_start;
                first_word_after          = new_header->image_end;
                const uint32_t first_page = flash_get_page_number(first_word);
                const uint32_t last_page  = flash_get_page_number(first_word_after - 1);
                for (uint32_t page = first_page; page <= last_page; page++) {
                    flash_erase_page(page);
                }
                LOG_INFO("pages %lu-%lu erased", first_page, last_page);

                start_flash_writer();

                // do NOT put the first chunk into the flash writer queue just yet; we'll do that at the very end
            } else {
                chunk->write_addr = (uint32_t*) ((uint32_t) first_chunk->write_addr + (uint32_t) request.offset);

                BaseType_t rv = xQueueSendToBack(flash_writer_queue, &chunk, portMAX_DELAY);
                ASSERT(rv == pdTRUE);
            }

            if (response.data.value.count < uavcan_primitive_Unstructured_1_0_value_ARRAY_CAPACITY_) {
                // this should have been the last chunk, wrap things up
                ASSERT(bytes_received == (uint32_t) new_header->image_end - (uint32_t) new_header->header_start);
                new_header = (ImageHeader_t*) first_chunk->write_addr;

                BaseType_t rv = xQueueSendToBack(flash_writer_queue, &first_chunk, portMAX_DELAY);
                ASSERT(rv == pdTRUE);

                break;
            }

            request.offset += response.data.value.count;
        }

        while (bytes_written < bytes_received) {
            // Wait for the Flash Writer task to finish.
            // Since the Flash Writer runs at idle priority (and thus at a priority lower than this task's) an active
            // delay is required. A busy wait loop would prevent the Flash Writer task from running and cause a deadlock
            // situation.
            vTaskDelay(10);
        }

        const TickType_t dfu_end         = xTaskGetTickCount();
        const uint32_t   dfu_duration_ms = (dfu_end - dfu_start) * 1000UL / configTICK_RATE_HZ;
        LOG_INFO("%lu bytes written in %lu ms", bytes_written, dfu_duration_ms);

        // the image has been (or should have been) fully written
        if (image_check(new_header, true, true)) {
            LOG_NOTICE("image verified, DFU successful");
            exit_DFU();
        }
        // else
        LOG_WARNING("DFU attempt %lu of %lu failed", dfu_attempt, MAX_DFU_ATTEMPTS);
    }

    LOG_ERROR("DFU failed");
    exit_DFU();
}

static void FlashWriterTask(void* param __unused) {
    ASSERT(flash_writer_queue != NULL);

    ImageChunk_t* chunk;

    while (1) {
        BaseType_t rv = xQueueReceive(flash_writer_queue, &chunk, portMAX_DELAY);
        ASSERT(rv == pdTRUE);

        ASSERT(chunk->write_addr >= first_word &&
               (uint32_t*) ((uint32_t) chunk->write_addr + chunk->size) <= first_word_after);

        LOG_DEBUG("write %luB to 0x%08lx", chunk->size, (uint32_t) chunk->write_addr);
        flash_write(chunk->write_addr, (uint32_t*) &chunk->data[0], chunk->size);
        bytes_written += chunk->size;
        node_modify_vssc(0xffu, bytes_written / 1024u);
        LOG_INFO("%luB written to 0x%08lx", chunk->size, (uint32_t) chunk->write_addr);
        vPortFree(chunk);

        // give the idle task a chance to run
        taskYIELD();
    }
}

static bool is_update_permitted(const ImageHeader_t* const old_header, const ImageHeader_t* const new_header) {
    // definitely no update if the new header is broken
    if (!image_check(new_header, true, false)) return false;

    const uint32_t* const new_image_start = new_header->header_start;
    const uint32_t* const new_image_end   = new_header->image_end;
    if (new_image_start < &__image_end || new_image_end >= &__flash_end__) {
        LOG_ERROR("Address violation");
        return false;
    }

    // This will need to change eventually
    if (new_header->image_hdr_version != eHEADER_VERSION_CURRENT) {
        LOG_ERROR("%s header: can't handle v%u", "new", new_header->image_hdr_version);
        return false;
    }

    if (dcb_validate()) {
        const char*  target = device_configuration_block.name;
        const size_t len    = strlen(target);
        if (strncmp(target, new_header->name, len) != 0) {
            LOG_ERROR("Target mismatch: %s does not fit %s", new_header->name, target);
            return false;
        }

        const bool hardware_compatible =
            *(uint16_t*) &device_configuration_block.release >= *(uint16_t*) &new_header->min_hardware &&
            *(uint16_t*) &device_configuration_block.release <= *(uint16_t*) &new_header->target_hardware;
        if (!hardware_compatible) {
            LOG_ERROR("Hardware mismatch: %c.%u not in %c.%u..%c.%u", device_configuration_block.release.major,
                      device_configuration_block.release.minor, new_header->min_hardware.major,
                      new_header->min_hardware.minor, new_header->target_hardware.major,
                      new_header->target_hardware.minor);
            return false;
        }
    } else {
        LOG_WARNING("DCB missing");
    }

    // if the update request was registered by the App Loader itself, we assume the update path is clear
    if (old_header == &image_hdr) return true;

    // no further restrictions if the old image is broken
    if (!image_check(old_header, true, true)) return true;

    // This will need to change eventually
    if (old_header->image_hdr_version != eHEADER_VERSION_CURRENT) {
        LOG_WARNING("%s header: can't handle v%u", "present", old_header->image_hdr_version);
        return true;
    }

    const bool update_step_permitted =
        *(uint32_t*) &old_header->version >= *(uint32_t*) &new_header->min_upgrade_from &&
        *(uint32_t*) &old_header->version <= *(uint32_t*) &new_header->max_downgrade_from;

    char old_image_version[VERSION_STRING_MAXSZ];
    image_render_version_string(old_image_version, &old_header->version, old_header->clean_build);

    if (!update_step_permitted) {
        char new_image_min[VERSION_STRING_MAXSZ], new_image_max[VERSION_STRING_MAXSZ];
        image_render_version_string(new_image_min, &new_header->min_upgrade_from, true);
        image_render_version_string(new_image_max, &new_header->max_downgrade_from, true);
        LOG_ERROR("Image %s not in permitted range %s..%s", old_image_version, new_image_min, new_image_max);
        return false;
    }

    char new_image_version[VERSION_STRING_MAXSZ];
    image_render_version_string(new_image_version, &new_header->version, new_header->clean_build);
    LOG_INFO("Update %s -> %s", old_image_version, new_image_version);

    return true;
}
