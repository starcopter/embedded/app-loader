/**
 * @file app-loader.c
 * @author Lasse Fröhner <lasse@starcopter.com>
 * @brief
 * @version 0.1
 * @date 2021-07-09
 *
 * @copyright Copyright (c) 2021 starcopter GmbH
 *
 * This software is distributed under the terms of the MIT License.
 *
 */

#include "main.h"
#include "dfu.h"

#include <canard/canard.h>
#include <canard/fdcan.h>
#include <canard/node.h>
#include <canard/transport.h>
#include <sclib/dcb.h>
#include <sclib/registry.h>

#include <FreeRTOS.h>
#include <task.h>

#include <sclib/console.h>
#define LOG_LEVEL LOG_LEVEL_TRACE
#include <sclib/log.h>

#include <uavcan/node/Mode_1_0.h>
#include <uavcan/node/Health_1_0.h>

static uint_fast8_t mode   = uavcan_node_Mode_1_0_INITIALIZATION;
static uint_fast8_t health = uavcan_node_Health_1_0_NOMINAL;

static void AppLoader(void* param);
static void print_platform_info(void);
static void try_launch_application(void);

void start_app_loader(void) {
    static TaskHandle_t handle = NULL;
    // This function may only be called once
    ASSERT(handle == NULL);
    // The task will directly enter running mode
    BaseType_t rv = xTaskCreate(AppLoader, "App Loader", 256, NULL, TaskPrioritySlow, &handle);
    ASSERT(rv == pdPASS);
}

uint_fast8_t get_system_mode(void) {
    const uint_fast8_t node_mode      = node_get_mode();
    const uint_fast8_t transport_mode = transport_get_mode();

    if (mode == uavcan_node_Mode_1_0_INITIALIZATION || transport_mode == uavcan_node_Mode_1_0_INITIALIZATION ||
        node_mode == uavcan_node_Mode_1_0_INITIALIZATION)
    {
        return uavcan_node_Mode_1_0_INITIALIZATION;
    }

    const uint_fast8_t ext_mode = node_mode > transport_mode ? node_mode : transport_mode;
    return ext_mode > mode ? ext_mode : mode;
}

uint_fast8_t get_system_health(void) {
    const uint_fast8_t transport_health = transport_get_health();
    return health > transport_health ? health : transport_health;
}

static void AppLoader(void* param) {
    // Assumptions:
    //   - the console (and logging module) has been initialized
    //   - the FDCAN module has been configured (peripheral enabled, clock selected, GPIOs set up)

    print_platform_info();

#if LOG_LEVEL <= LOG_LEVEL_TRACE
    const char* literal_app_loader = "App Loader";
    uint32_t    boot_count         = shared_get_boot_count() - 1;
    if (boot_count) {
        LOG_TRACE("%s hot boot (%u)", literal_app_loader, (uint8_t) boot_count);
    } else {
        LOG_TRACE("%s cold boot", literal_app_loader);
    }
#endif

    const bool dfu_requested = shared_dfu_requested();

    if (!dfu_requested) {
        try_launch_application();
        LOG_WARNING("No valid app found");
    }

    /* ---------------------------------------------------------------- */

    {
        const UAVCANRegister_t* const reg = get_register_by_name("uavcan.node.id");
        ASSERT(reg);
        uint16_t* nid = (uint16_t*) reg->value;
        if (*nid > CANARD_NODE_ID_MAX) {
            // semi-random node ID between 1 and 125, both inclusive
            *nid = UID->WORD1 % 125 + 1;
        }
    }

    node_init();

    /* ---------------------------------------------------------------- */

    mode   = uavcan_node_Mode_1_0_SOFTWARE_UPDATE;
    health = dfu_requested ? uavcan_node_Health_1_0_NOMINAL : uavcan_node_Health_1_0_WARNING;

    if (dfu_requested) start_DFU();

    // nothing else to be done
    shared_reset_boot_count();

    for (;;) {
        if (!dfu_requested) {
            // a new DFU request will reset the node
            LOG_INFO("Waiting for DFU Request...");
        }
        vTaskDelay(pdMS_TO_TICKS(1000));
    }
}

static void print_platform_info(void) {
    const char* const name = image_hdr.name;
    char              image_version_string[VERSION_STRING_MAXSZ];
    image_render_version_string(image_version_string, &image_hdr.version, image_hdr.clean_build);
    const HWVersion_t* const hw = &device_configuration_block.release;
    LOG_NOTICE("%s %s on HW %c.%u%s", name, image_version_string, hw->major, hw->minor, dcb_validate() ? "" : " (?)");

    extern const ELFNote_t g_note_build_id;
    LOG_INFO("Git Rev %08lx%s, Build ID %08lx, CRC %08lx/%08lx", __builtin_bswap32(*(uint32_t*) &image_hdr.git_sha[0]),
             image_hdr.clean_build ? "" : "*",
             __builtin_bswap32(*(uint32_t*) &g_note_build_id.data[g_note_build_id.namesz]), image_hdr.header_crc,
             __image_crc);
    LOG_INFO("Running on device %08lx", __builtin_bswap32(*(uint32_t*) &device_configuration_block.uid[12]));
}

static void try_launch_application(void) {
    extern ApplicationContext_t* _app_loader_get_launch_context(void);
    ApplicationContext_t*        ctx = _app_loader_get_launch_context();

    if (ctx->application == NULL) {
        // this is a cold boot
        ctx->application = (ImageHeader_t*) (((uint32_t) &__image_end - 1 + PAGE_SIZE) & ~((uint32_t) PAGE_SIZE - 1));
        ctx->boot_count  = 0;
        LOG_TRACE("cold boot");
    }

    while (ctx->application >= (ImageHeader_t*) &__image_end && ctx->application < (ImageHeader_t*) &__nvm_start__) {
        if (image_check(ctx->application, true, true) && ctx->boot_count <= 3) {
            shared_reset_boot_count();  // last chance
            LOG_DEBUG("booting (%u) image at 0x%08lx", (uint8_t) ctx->boot_count + 1, (uint32_t) ctx->application);
            SysTick->CTRL = 0;  // this should kill the FreeRTOS scheduler
            deinit_app_loader();
            image_boot(ctx);  // never returns
        }
        ctx->boot_count  = 0;
        ctx->application = (ImageHeader_t*) ((uint32_t) ctx->application + PAGE_SIZE);
    }

    // we've reached the end of our application space, prepare for eventual reboot
    ctx->application = (ImageHeader_t*) (((uint32_t) &__image_end - 1 + PAGE_SIZE) & ~((uint32_t) PAGE_SIZE - 1));
    ctx->boot_count  = 0;
}
