/**
 * @file main.h
 * @author Lasse Fröhner <lasse@starcopter.com>
 * @brief Application Loader
 * @version 0.1
 * @date 2023-03-30
 *
 * @copyright Copyright (c) 2023 starcopter GmbH
 *
 * This software is distributed under the terms of the MIT License.
 *
 */

#pragma once

#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <assert.h>  // For static_assert (C11)
#include <stddef.h>

#include <device.h>

#define TASK_PRIORITIES 8

typedef enum TaskPriority {
    TaskPriorityExceptional = 7,
    TaskPriorityImmediate   = 6,
    TaskPriorityFast        = 5,
    TaskPriorityHigh        = 4,
    TaskPriorityNominal     = 3,
    TaskPriorityLow         = 2,
    TaskPrioritySlow        = 1,
    TaskPriorityIdle        = 0,
} TaskPriority;

#define configTRANSPORT_TASK_PRIORITY            TaskPriorityImmediate
#define configTRANSPORT_BLOCK_TIME_MS            100
#define configNODE_GET_INFO_TASK_PRIORITY        TaskPriorityLow
#define configNODE_EXECUTE_COMMAND_TASK_PRIORITY TaskPriorityLow

#if __NVIC_PRIO_BITS != 4
#    error This port is configured for 16 interrupt priority levels
#endif

typedef enum InterruptPriority {
    InterruptPriorityExceptional = 0,

    // highest interrupt priority from which interrupt safe FreeRTOS API functions can be called
    InterruptPriorityMaxSyscall = 4,

    InterruptPriorityFDCAN = 6,

    InterruptPriorityTimekeeper = 8,

    InterruptPriorityUARTConsole = 14,
    // interrupt priority used by the RTOS kernel itself, should be set to lowest priority
    InterruptPriorityKernel = 15,
} InterruptPriority;

#define configFDCAN_IT1_PRIORITY                  InterruptPriorityFDCAN
#define configTIMEKEEPER_ISR_PRIORITY             InterruptPriorityTimekeeper
#define configTIMEKEEPER_INCLUDE_NETWORK_TIMESYNC 0
#define configTIMEKEEPER_PUBLISH_STATUS_MESSAGE   0
#define configSTDOUT_USART                        USART2
#define configSTDOUT_USART_IRQn                   USART2_IRQn
#define configSTDOUT_USART_ISR_PRIORITY           InterruptPriorityUARTConsole
#define configSTDOUT_BUFFERSIZE                   1024

#include <sclib/assert.h>
#include <sclib/memory_map.h>
#include <sclib/image.h>
#include <sclib/shared_ram.h>

#include <FreeRTOS.h>


extern ImageHeader_t image_hdr;

void init_app_loader(void);
void start_app_loader(void);
void deinit_app_loader(void);

#define configREGISTRY_LOG_LEVEL LOG_LEVEL_INFO

#define configREGISTRY_INCLUDE_SERVICES                  0
#define configREGISTRY_INCLUDE_SAVE_PERSISTENT_REGISTERS 0
#define configREGISTRY_INCLUDE_LOAD_PERSISTENT_REGISTERS 1
